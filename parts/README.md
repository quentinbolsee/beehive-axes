# Beehive machine building kit: parts

## Guide

Here you can find the files for the whole kit, structured as follows:

- **bare_endcaps**: endcaps for all types of beam-beam joinery
- **beam**: 20mm extrusion, 3D printed substitutes
- **carriages**: wheel-based carriage that slides on a beam
- **bare_attachments**: Attachments that can mount on endcaps or carriages
- **combined_attachments-endcaps**: combination of an endcap and an attachment (not all combinations are present here)
- **interposers**: joinery for the M5, 10mm spacing hole grid
- **capstan_mtion**: anything related to string-based motion

## Overview

### Beam

![](../overview/beam.png)

### Endcaps

![](../overview/endcap.png)

### Attachments, bare

![](../overview/bare_attachments.png)

### Attachments, combined

![](../overview/combined_attachments.png)

### Interposers

![](../overview/interposers.png)

### Carriage

![](../overview/carriage.png)
