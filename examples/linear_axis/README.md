# Beehive linear axis example

![](img/axis.jpg)

This quick guide provides a recipe for a simple linear axis, given the kit parts provided [here](../../parts).

## Bill of Materials

Attachments:

- 1x `motor_endcap_short`
- 1x `55x55x35_endcap_short`

Carriage and beam:

- 1x `carriage_string`
- 120mm long, 20x20mm extrusion

Interposers (feet):

- 2x `inter_40mm_90`

String carriage specific:

- 1x `pulley`
- 1x `pulley_holder`
- 1x `capstan_pulley`
- 1x `capstan_motor`
- 1x `capstan_holder`

Hardware:

- 5x 40mm M5 socket head
- 15x 10mm M5 socket head
- 8x 8mm M3 button head
- 5x M5 nuts
- 2x M3 nuts
- 4x Derlin V-wheel
- 3x 625-2RS bearing
- 6x T-slot nuts or M5 nuts (for a printed beam)

## Assembly

The carriage is the first part to go on the beam. Two M3 screws are used to attach the string ends; for now only one of them should be fastened.

![](img/carriage.jpg)

At the bottom, 4 M5 nuts secure the derlin wheels:

![](img/carriage_under.jpg)

The motor is fastened on the motor attachment using 4 M3 screws.

![](img/motor.jpg)

For the capstan pulley that sits next to the motor shaft, you should insert two 625-2RS bearings separated by the spacer, this prevents excessive compression on the bearing pair.

![](img/capstan_spacer.jpg)

The string is wrapped bottom to top. It's easier to mount the capstan support structure only at the end. Note the two M3 screws + nuts that fasten the motor's capstan to the shaft.

![](img/capstan.jpg)

On the other end, the pulley is assembled by press-fitting a 625-2RS bearing and assembling the pulley holder onto the grid of holes. Note the slots for tensioning as the last step.

![](img/pulley.jpg)

The complete axis should look something like this after adding the feet. Note how the free side of the string passes through the hole embedded within the carriage:

![](img/axis.jpg)
