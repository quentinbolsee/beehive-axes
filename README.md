# Beehive machine building kit

<img src=./img/beehive_logo.png width=35%><br>

This project provides a series of modular parts that can be easily composed to make linear motion axes and machine components. The core of the project is based on:

- Aluminum extrusions, or [3D printed alternative](./#beam)
- Derlin wheel carriages
- Grid of tap-in-plastic M5 holes with 10mm spacing

The project is named after the grid of holes that can either be busy or free, much like cells in a beehive.

Here's an example of string-carried linear axis using only beehive parts:

[![](examples/linear_axis/img/axis.jpg)](./examples/linear_axis)

## Terminology

### Beam

A beam serves as a linear structural element, as well as a guide for a linear motion system. Standard 20mm alluminum extrusions are compatible, but you can also choose to 3D print your own custom beam.

![](./overview/beam.png)

### Endcaps

An endcap sits on the end of a beam and can join several of them.

![](./overview/endcap.png)

### Attachments

Attachments offer specific functionality (e.g. hosting a motor, a pulley or a toolhead) and offer a grid of M5, 10mm spaced hole for easier composability:

![](./overview/bare_attachments.png)

They can be combined with an endcap to sit on the end of a beam, here are some examples:

![](./overview/combined_attachments.png)

### Interposers

Interposers sit between two attachments, and offer pass-through M5 holes with a 10mm spacing.

![](./overview/interposers.png)

### Carriage

A carriage sits on a beam and can slide onto it. They are typically carried by a string or a timing belt.

![](./overview/carriage.png)

## Dimensions

### Grid of holes

The grid of hole present on any attachment is an M5, 10mm spacing grid.

### Endcaps

The end of a beam presents a docking zone for an endcap, following these rules:

- 30mm length
- Two M5 pass-through holes, distanced by 12mm
- Three faces present M5 holes, one doesn't

Endcaps can use any of the 6 holes for fastening.

## Example

A complete implementation of a linear axis is presented [here](./examples/linear_axis)

## Files

The files can be found [here](./parts), provided in the following format:

- Fusion archive (.f3d)
- Step file (.step)
- 3D print file (.stl)
